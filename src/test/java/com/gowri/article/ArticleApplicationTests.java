package com.gowri.article;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.json.JSONException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.gowri.article.dto.ArticleRequestDto;
import com.gowri.article.dto.ArticleResponseDto;
import com.gowri.article.dto.TagSummaryDto;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ArticleApplicationTests {

	
	
    @LocalServerPort
    private int port;
    private TestRestTemplate restTemplate = new TestRestTemplate();
    private HttpHeaders headers = new HttpHeaders();
    
    @Test
    public void testPersistArticles() throws IOException, JSONException{

    	getArticleRequest1().stream().forEach(m -> getArticleResponseDto(m));
        
    	ArticleResponseDto articleResponseDto = getArticle("articles/02");	
    	Assert.assertEquals(new Long(2), articleResponseDto.getId());

    	Assert.assertEquals(ArticleRequest2.getTitle(), articleResponseDto.getTitle());
    	Assert.assertEquals(ArticleRequest2.getBody(), articleResponseDto.getBody());
   
    	Assert.assertEquals(ArticleRequest2.getDate(), articleResponseDto.getDate());
    	Assert.assertEquals(ArticleRequest2.getTags(), articleResponseDto.getTags());
    }
    
    @Test
    public void testTagSummaryCount() throws IOException, JSONException{
    	getArticleRequest1().stream().forEach(m -> getArticleResponseDto(m));
    	TagSummaryDto tagSummaryDto =  getTagSummaryDto("/tags/health/20160922");
    	Assert.assertEquals(new Long(4), (Long)tagSummaryDto.getCount());
    	Assert.assertEquals(6, tagSummaryDto.getRelatedTags().size());
    	Assert.assertEquals(4, tagSummaryDto.getArticles().size());
    	
    	Assert.assertEquals(true, tagSummaryDto.getRelatedTags().contains("science"));
   
     	Assert.assertEquals(true, tagSummaryDto.getRelatedTags().contains("maths"));
    	
    	Assert.assertEquals(true, tagSummaryDto.getRelatedTags().contains("theatre"));
    	Assert.assertEquals(true, tagSummaryDto.getRelatedTags().contains("movies"));
    	Assert.assertEquals(true, tagSummaryDto.getRelatedTags().contains("food"));
     	Assert.assertEquals(true, tagSummaryDto.getRelatedTags().contains("fitness"));
    }
    
    
    public ArticleRequestDto buildArticleRequestDto (String title, String date, String body, String[] tags) {
    	LocalDate articleDate = LocalDate.parse(date);
     	List<String> articleTags = Arrays.asList(tags);   	
    	return new ArticleRequestDto(title, articleDate, body, articleTags);
    }
    
    public ArticleResponseDto getArticleResponseDto(ArticleRequestDto articleRequestDto){
        HttpEntity<ArticleRequestDto> entity = new HttpEntity<ArticleRequestDto>(articleRequestDto, headers);

        ResponseEntity<ArticleResponseDto> response = restTemplate.exchange(
                createURLWithPort("/articles"),
                HttpMethod.POST, entity, ArticleResponseDto.class);
        return response.getBody();
    }
    
    public TagSummaryDto getTagSummaryDto(String url){
        HttpEntity<ArticleRequestDto> entity = new HttpEntity<ArticleRequestDto>(headers);

        ResponseEntity<TagSummaryDto> response = restTemplate.exchange(
                createURLWithPort(url),
                HttpMethod.GET, entity, TagSummaryDto.class);
        return response.getBody();
    }
    
    public ArticleResponseDto getArticle (String url){
        HttpEntity<ArticleRequestDto> entity = new HttpEntity<ArticleRequestDto>(headers);

        ResponseEntity<ArticleResponseDto> response = restTemplate.exchange(
                createURLWithPort(url),
                HttpMethod.GET, entity , ArticleResponseDto.class);
        return response.getBody();
    }
    
    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }
    
    public List<ArticleRequestDto> getArticleRequest1() {
    
    	return Arrays.asList(ArticleRequest1,ArticleRequest2, 
    			ArticleRequest3, ArticleRequest4, 
    			ArticleRequest5, ArticleRequest6 );
    }
	@Test
	public void contextLoads() {
	}

	
	private ArticleRequestDto ArticleRequest1 = buildArticleRequestDto("title 1", "2016-09-22", 
			"some text, potentially containing simple markup about how potato chips are great",
			new String[] {"health", "fitness", "science"});
	
	private ArticleRequestDto ArticleRequest2 = buildArticleRequestDto("title 2", "2016-09-22", 
			"some text2, potentially containing simple markup about how potato chips are great",
			new String[] {"health", "movies", "theatre"});
	
	private ArticleRequestDto ArticleRequest3 = buildArticleRequestDto("title 3", "2016-09-22", 
			"some text3, potentially containing simple markup about how potato chips are great",
			new String[] {"theatre", "health", "food"});
	
	private ArticleRequestDto ArticleRequest4 = buildArticleRequestDto("title 4", "2016-09-22", 
			"some text4, potentially containing simple markup about how potato chips are great",
			new String[] {"science", "maths", "health"});
	
	private ArticleRequestDto ArticleRequest5 = buildArticleRequestDto("title 5", "2016-09-22", 
			"some text5, potentially containing simple markup about how potato chips are great",
			new String[] {"chemistry", "physics", "nature"});
	
	private ArticleRequestDto ArticleRequest6 = buildArticleRequestDto("title 6", "2016-09-20", 
			"some text6, potentially containing simple markup about how potato chips are great",
			new String[] {"chemistry", "physics", "nature"});

	
}
