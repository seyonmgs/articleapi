/**
 * 
 */
package com.gowri.article.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.gowri.article.dto.ArticleRequestDto;
import com.gowri.article.dto.ArticleResponseDto;
import com.gowri.article.model.Article;
import com.gowri.article.model.Tag;
import com.gowri.article.service.ArticleService;
import com.gowri.article.service.TagService;

/**
 * @author Gowri
 *
 */

@RestController
public class ArticleController extends BaseController {
	
	@Autowired
	private ArticleService articleService;

	@Autowired
	private TagService tagService;

	@GetMapping("/articles/{id}")
	public ArticleResponseDto getArticles(@PathVariable (value = "id") Long id) {
		
		ArticleResponseDto articleResponseDto= convertToArticleResponseDto(articleService.findArticle(id));
		List<String> tags =tagService.findAllTagsOfAnArticle(id).stream().map(tagEntry -> tagEntry.getTagName())
		.collect(Collectors.toList());	 
		articleResponseDto.setTags(tags);
		return articleResponseDto;

	}
	
	@PostMapping("/articles")
	public ArticleResponseDto addArticle(@Valid @RequestBody ArticleRequestDto articleRequestDto) {
		Article article = articleService.addArticle(convertToArticle(articleRequestDto));
		List<String> tagNames = articleRequestDto.getTags().stream().distinct().collect(Collectors.toList());
		List<Tag> tags = tagService.addTagsForAnArticle(article, tagNames);
		return convertToArticleResponseDtoWithTags(article, tags);
	}
	
	@PutMapping("/articles/{id}")
	public ArticleResponseDto updateArticle(@PathVariable(value = "id") Long id, @Valid @RequestBody ArticleRequestDto articleRequestDto) {
		return convertToArticleResponseDto(articleService.updateArticle(id, convertToArticle(articleRequestDto)));
	}		
	
	@DeleteMapping("/articles/{id}")
	public ResponseEntity<?> deleteArticle(@PathVariable(value = "id") Long id) {
		return articleService.deleteArticle(id);
	}

	

	
	
}
