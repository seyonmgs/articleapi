package com.gowri.article.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.gowri.article.dto.ArticleRequestDto;
import com.gowri.article.dto.ArticleResponseDto;
import com.gowri.article.dto.TagResponseDto;
import com.gowri.article.model.Article;
import com.gowri.article.model.Tag;

public abstract class BaseController {
	
	@Autowired
	public ModelMapper modelMapper;

	public  Article convertToArticle (ArticleRequestDto articleRequestDto) {	
		Article article = modelMapper.map(articleRequestDto, Article.class);
		article.setDateOfArticle(articleRequestDto.getDate());
		return article;
	}
	
	public  ArticleResponseDto convertToArticleResponseDto(Article article){
		return modelMapper.map(article, ArticleResponseDto.class);
	}
	
	public  ArticleResponseDto convertToArticleResponseDtoWithTags (Article article, List<Tag> tags) {	
		ArticleResponseDto articleResponseDto = convertToArticleResponseDto(article);		
		List<String> tagsOftheArticle = tags.stream().map(m -> m.getTagName()).collect(Collectors.toList());
		articleResponseDto.setTags(tagsOftheArticle);
		return articleResponseDto;
	}
	
	
	public TagResponseDto convertToTagResponseDto(Tag tag){
		TagResponseDto tagResponseDto= modelMapper.map(tag, TagResponseDto.class);
		tagResponseDto.setArticleId(tag.getArticle().getId());
		return tagResponseDto;
	}
	
	public LocalDate urlDateFormatter(String s) {
		return LocalDate.parse(s, DateTimeFormatter.BASIC_ISO_DATE );
	}
}
