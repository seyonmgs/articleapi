/**
 * 
 */
package com.gowri.article.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.gowri.article.dto.TagRequestDto;
import com.gowri.article.dto.TagResponseDto;
import com.gowri.article.dto.TagSummaryDto;
import com.gowri.article.service.TagService;

/**
 * @author Gowri
 *
 */

@RestController
public class TagController extends BaseController {

	@Autowired
	private TagService tagService;

	@GetMapping("/tags/{tagName}/{date}")
	public TagSummaryDto getTagsSummary(@PathVariable(value = "tagName") String tagName,
			@PathVariable(value = "date") String date) {
		return tagService.getSummaryForTagOnDateRequested(urlDateFormatter(date), tagName);

	}
	
	@GetMapping("/articles/{id}/tags")
	public List<TagResponseDto> getTags(@PathVariable(value = "id") Long articleId) {
		return tagService.findAllTagsOfAnArticle(articleId).stream().map(m -> convertToTagResponseDto(m))
				.collect(Collectors.toList());
	}

	@GetMapping("/articles/tags/{tagName}/{date}")
	public List<TagResponseDto> getTags(@PathVariable(value = "tagName") String tagName,
			@PathVariable(value = "date") String date) {
		return tagService.findByDateOfArticleAndTag(urlDateFormatter(date), tagName).stream()
				.map(m -> convertToTagResponseDto(m)).collect(Collectors.toList());
	}


	@PutMapping("/articles/{id}/tags")
	public List<TagResponseDto> updateTagsForArcticle(@PathVariable(value = "id") Long articleId,
			@Valid @RequestBody TagRequestDto tagRequestDto) {
		return tagService.updateTagsForAnArticle(articleId, tagRequestDto.getTag()).stream()
				.map(m -> convertToTagResponseDto(m)).collect(Collectors.toList());
	}
	

	
	@DeleteMapping("/articles/{id}/tags")
	public ResponseEntity<?> deleteArticle(@PathVariable(value = "id") Long articleId) {
		return tagService.deleteTagsForArticle(articleId);
	}

	
}
