package com.gowri.article.configuration;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class ArticleConfiguration {
   
    @Bean
    public ModelMapper modelMapper() {return new ModelMapper();} 

}
