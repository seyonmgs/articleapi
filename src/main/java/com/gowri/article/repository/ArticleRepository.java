package com.gowri.article.repository;

import org.springframework.data.repository.CrudRepository;

import com.gowri.article.model.Article;

public interface ArticleRepository extends CrudRepository<Article, Long> {

}
