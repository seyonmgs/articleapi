package com.gowri.article.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gowri.article.model.Tag;

public interface TagRepository extends CrudRepository<Tag, Long> {

	public List<Tag> findByArticleId (long articleId);
	
    public void deleteByArticleId (long articleId);
	
	public List<Tag> findByDateOfArticleAndTagName (LocalDate dateOfArticle, String tag);
	
	public Long countByDateOfArticleAndTagName(LocalDate dateOfArticle, String tag);
	
	public List<Tag> findByArticleIdIn(List<Long> articleIds);
	
	public Long countByDateOfArticle (LocalDate dateOfArticle);
	
	public List<Tag> findTop10ByDateOfArticleAndTagNameOrderByCreatedDesc (LocalDate dateOfArticle, String tag);
}
