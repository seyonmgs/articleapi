package com.gowri.article.model;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table (name = "tags")

public class Tag extends AuditEntity {
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private long id;
	
	@NotNull
	@OnDelete(action = OnDeleteAction.CASCADE)
	@ManyToOne (fetch = FetchType.LAZY, optional = false)
	@JoinColumn (name = "article_id", nullable = false)
	private Article article;
	
	@DateTimeFormat
	private LocalDate dateOfArticle;
	
	@Length(min = 1, message = "tag should atleast be one character long" )
	@Length(max = 30, message = "tag should at max be 30 characers long" )
	@NotNull
	private String tagName;
	
	
	

}
