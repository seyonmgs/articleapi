package com.gowri.article.dto;

import java.io.Serializable;
import java.time.LocalDate;

import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@SuppressWarnings("serial")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TagResponseDto implements Serializable {
	
	
	@NotNull
	private long id;
	@NotNull
	private long articleId;
	
	@NotNull
	@DateTimeFormat
	private LocalDate dateOfArticle;
	
	@NotNull
	private String tag;

}
