package com.gowri.article.dto;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@SuppressWarnings("serial")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class TagSummaryDto implements Serializable {

	@NotNull
	private String tag;
	@PositiveOrZero
	private long count;
	private List<Long> articles;
	@JsonProperty("related_tags")
    private List<String> relatedTags;	
}
