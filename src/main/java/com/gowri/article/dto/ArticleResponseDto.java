package com.gowri.article.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@SuppressWarnings("serial")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)

public class ArticleResponseDto implements Serializable {
	

	private Long id;
	@NotNull
	private String title;
	@NotNull
	@DateTimeFormat
	private LocalDate date;
	@NotNull
	private String body;
	
	private List<String> tags;



}
