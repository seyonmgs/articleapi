package com.gowri.article.dto;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@SuppressWarnings("serial")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TagRequestDto implements Serializable {
	
	
	@NotNull
	@Min(value = 1)
	private long articleId;
	
	@NotNull
	private List<String> tag;

}
