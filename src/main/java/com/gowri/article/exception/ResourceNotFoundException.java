package com.gowri.article.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException{

	public ResourceNotFoundException() {
		super ();
	}
	
	public ResourceNotFoundException(String error) {
		super (error);
	}
	
	public ResourceNotFoundException(String error, Throwable reason) {
        super(error, reason);
    }
	
}
