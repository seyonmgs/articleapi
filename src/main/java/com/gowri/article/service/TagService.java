/**
 * 
 */
package com.gowri.article.service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gowri.article.dto.TagSummaryDto;
import com.gowri.article.exception.ResourceNotFoundException;
import com.gowri.article.model.Article;
import com.gowri.article.model.Tag;
import com.gowri.article.repository.ArticleRepository;
import com.gowri.article.repository.TagRepository;

/**
 * @author Gowri
 *
 */
@Transactional
@Service
public class TagService {

	@Autowired
	private TagRepository tagRepository;

	@Autowired
	private ArticleRepository articleRepository;

	public List<Tag> findAllTagsOfAnArticle(long articleId) {

		return tagRepository.findByArticleId(articleId);

	}

	public List<Tag> findByDateOfArticleAndTag(LocalDate dateOfArticle, String tag) {
		return tagRepository.findByDateOfArticleAndTagName(dateOfArticle, tag);
	}

	public TagSummaryDto getSummaryForTagOnDateRequested(LocalDate dateOfArticle, String tag) {

		List<Tag> tagsFortheTagOnDateRequested = tagRepository.findByDateOfArticleAndTagName(dateOfArticle, tag);

		long count = tagsFortheTagOnDateRequested.size();

		List<Long> allArticlesTaggedOnDateRequested = getDistinctArticleIdsFromTags(tagsFortheTagOnDateRequested);

		List<Long> last10ArticlesTaggedOnDateRequested = getDistinctArticleIdsFromTags(
				tagRepository.findTop10ByDateOfArticleAndTagNameOrderByCreatedDesc(dateOfArticle, tag));

		List<String> relatedTags = tagRepository.findByArticleIdIn(allArticlesTaggedOnDateRequested).stream()
				.map(tagEntry -> tagEntry.getTagName()).distinct().filter(tagName -> (!(tagName).equals(tag)))
				.collect(Collectors.toList());

		return new TagSummaryDto(tag, count, last10ArticlesTaggedOnDateRequested, relatedTags);
	}

	public List<Tag> addTagsForAnArticle(Article article, List<String> tags) {

		LocalDate dateOfArticle = article.getDateOfArticle();
		return tags.stream().distinct().map(m -> mapATag(dateOfArticle, article, m)).map(m -> addTag(m))
				.collect(Collectors.toList());

	}

	public List<Tag> updateTagsForAnArticle(long articleId, List<String> tags) {

		tagRepository.deleteByArticleId(articleId);
		return addTagsForAnArticle(articleRepository.findById(articleId).get(), tags);

	}

	public Tag addTag(Tag tag) {
		return tagRepository.save(tag);
	}

	public ResponseEntity<?> deleteTagsForArticle(Long articleId) {
		if (tagRepository.findByArticleId(articleId).size() <= 0) {
			throw new ResourceNotFoundException("Article id " + articleId + "is not found.  Tag Cannot be deleted");
		}
		tagRepository.deleteByArticleId(articleId);
		return ResponseEntity.ok().build();
	}

	private Tag mapATag(LocalDate dateOfArticle, Article article, String tagText) {
		Tag tag = new Tag();
		tag.setArticle(article);
		tag.setDateOfArticle(dateOfArticle);
		tag.setTagName(tagText);
		return tag;
	}

	private List<Long> getDistinctArticleIdsFromTags(List<Tag> tags) {
		return tags.stream().map(tagEntry -> tagEntry.getArticle().getId()).distinct().collect(Collectors.toList());

	}

}
