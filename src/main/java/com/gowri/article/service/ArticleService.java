/**
 * 
 */
package com.gowri.article.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gowri.article.exception.ResourceNotFoundException;
import com.gowri.article.model.Article;
import com.gowri.article.repository.ArticleRepository;

/**
 * @author Gowri
 *
 */
@Transactional
@Service
public class ArticleService {
	
	@Autowired
	private ArticleRepository articleRepository;
	
	public Article addArticle(Article article) {
		
		return articleRepository.save(article);
		
	}

	public Article updateArticle (Long id, Article updatedArticle) {
		
		return articleRepository.findById(id).map(article -> {
			article.setBody(updatedArticle.getBody());
			article.setTitle(updatedArticle.getTitle());
			return articleRepository.save(article);
		}).orElseThrow(() -> new ResourceNotFoundException("Article id " + id + "is not found. Can not be updated"));

	}
	
	public ResponseEntity<?> deleteArticle(Long id) {
	
		return articleRepository.findById(id).map(article -> {
			articleRepository.delete(article);
			return ResponseEntity.ok().build();
		}).orElseThrow(() -> new ResourceNotFoundException("Article id " + id + "is not found.  Can not be deleted"));
	}
	
	public Article findArticle(Long id) {
		
		return articleRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Article id " + id + "is not found.  Can not be deleted"));
		

	}
	
	
	
}
