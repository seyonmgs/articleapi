FROM openjdk:8
ADD target/article-api-sub.jar article-api-sub.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "article-api-sub.jar"]